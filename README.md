### Installing

Clone this repo and install the project's dependencies by running:

```
yarn
```

## Run

To start server, run:

```
yarn start
```
