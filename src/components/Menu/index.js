import React from 'react'
import { 
    AppBar,
    Toolbar,
    Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  menu: {
    cursor: 'pointer',
    marginRight: '20px',
    textDecoration: 'none',
    color: '#fff',

    '&:hover': {
      textShadow: '0 0 4px #fff',
    },
  }
}));

const Menu = () => {
  const classes = useStyles();
  return (
    <AppBar position="relative">
        <Toolbar>
          <a className={classes.menu}  href='/'>
            <Typography variant="h6" color="inherit" noWrap>
              Home
            </Typography>
          </a>
          <a className={classes.menu} href='/results'>
            <Typography variant="h6" color="inherit" noWrap>
              Resultados
            </Typography>
          </a>
        </Toolbar>
    </AppBar>
  )
};

export default Menu;
