import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {
    List,
    ListItem,
    ListItemText,
    ListItemAvatar,
    Typography,
    Paper,
} from '@material-ui/core';
import Rating from '@material-ui/lab/Rating';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    container: {
        padding: '50px 10px 0px 10px',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
    },
    titleContainer: {
        padding: theme.spacing(8, 0, 6),
    },
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
        '@media (max-width:780px)': {
            padding: 0,
        }
    },
    listItem: {
        padding: '0',
        minHeight: '150px',

        '@media (max-width:780px)': {
            padding: 10,
        }
    },
    paper: {
        padding: 20,
        maxWidth: 360,
    },
    rating: {
        display: 'flex',
        justifyContent: 'center',
    },
    evaluation: {
        marginLeft: '10px',
    },
    image: {
        maxWidth: '100px',
        minWidth: '100px',
        padding: '20px',
    }
  }));

const Ranking = ({ startups, title }) => {
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <Typography gutterBottom variant="h5" component="h2">
                {title}
            </Typography>
            <Paper className={classes.paper}>
                <List className={classes.root}>
                    {
                        startups &&
                        startups.map((startup, idx) => (    
                            <Fragment key={startup.name} >
                                <ListItem className={classes.listItem}>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {`${idx + 1}º`}
                                    </Typography>
                                    <ListItemAvatar>
                                        <img className={classes.image} src={startup.imageUrl} alt=''/>
                                    </ListItemAvatar>
                                    <div>
                                        <ListItemText primary={startup.name} secondary={startup.segment} />
                                        <div className={classes.rating}>
                                            <Rating
                                                precision={0.1}
                                                name='proposal'
                                                value={parseFloat(startup.proposal.evaluation.toFixed(1))}
                                                readOnly
                                            />
                                            <Typography className={classes.evaluation} gutterBottom variant="h5" component="h2">
                                                {`${startup.proposal.evaluation.toFixed(1)} / 5`}
                                            </Typography>
                                        </div>
                                    </div>
                                </ListItem>
                            </Fragment>
                        ))
                    }
                </List>
            </Paper>
        </div>
    )
};

Ranking.defaultProps = {
    title: '',
}

Ranking.propTypes = {
    startups: PropTypes.array.isRequired,
    title: PropTypes.string,
}

export default Ranking;