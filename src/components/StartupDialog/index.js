import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { 
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    Typography,
} from '@material-ui/core';
import Rating from '@material-ui/lab/Rating';
import { makeStyles } from '@material-ui/core/styles';
import { db } from '../../config/firebase';

const useStyles = makeStyles(() => ({
    image: {
        marginLeft: 'auto',
        marginRight: 'auto',
        display: 'block',
        width: '50%',
        padding: '20px',
    },  
    name: {
        display: 'flex',
        justifyContent: 'center',
    },
    segment: {
        display: 'flex',
        justifyContent: 'center',
        paddingBottom: '15px',
    },
    ratingGroup: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '15px',
    },
    criterion: {
        marginTop: '25px',
    }, 
}));

const StartupDialog = ({ open, startup, onClose }) => {
    const classes = useStyles();
    const [startupData, setStartupData] = useState({});
    const [evaluation, setEvaluation] = useState({
        proposal: 0,
        pitch: 0,
        development: 0,
    });

    useEffect(() => {
        startup.name &&
        db.collection('startups').doc(startup.name).get()
        .then(doc => {
            setStartupData(doc.data());
        });
    }, [startup, startup.name, open]);

    const handleClose = () => {
        onClose();
        setEvaluation({
            ...evaluation,
            proposal: 0,
            pitch: 0,
            development: 0,
        });
    }

    const handleSave = () => {
        db.collection('startups').doc(startup.name)
        .set(
            {
                name: startup.name,
                segment: startup.Segment.name,
                imageUrl: startup.imageUrl,
                proposal: { 
                    sum: startupData?.proposal?.sum ? startupData.proposal.sum + evaluation.proposal : evaluation.proposal,
                    totalEvaluations: startupData?.proposal?.totalEvaluations ? startupData.proposal.totalEvaluations + 1 : 1,
                    evaluation: (startupData?.proposal?.sum && startupData?.proposal?.totalEvaluations) ? (startupData.proposal.sum + evaluation.proposal)/(startupData.proposal.totalEvaluations + 1) : evaluation.proposal,
                },
                pitch: { 
                    sum: startupData?.pitch?.sum ? startupData.pitch.sum + evaluation.pitch : evaluation.pitch,
                    totalEvaluations: startupData?.pitch?.totalEvaluations ? startupData.pitch.totalEvaluations + 1 : 1,
                    evaluation: (startupData?.pitch?.sum && startupData?.pitch?.totalEvaluations) ? (startupData.pitch.sum + evaluation.pitch)/(startupData.pitch.totalEvaluations + 1) : evaluation.pitch,
                },
                development: { 
                    sum: startupData?.development?.sum ? startupData.development.sum + evaluation.development : evaluation.development,
                    totalEvaluations: startupData?.development?.totalEvaluations ? startupData.development.totalEvaluations + 1 : 1,
                    evaluation: (startupData?.development?.sum && startupData?.development?.totalEvaluations) ? (startupData.development.sum + evaluation.development)/(startupData.development.totalEvaluations + 1) : evaluation.development,
                },
            }
        );
        handleClose();
    };

    return (
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
            <DialogContent>
                <img className={classes.image} src={startup.imageUrl} alt={startup.name} />
                <Typography className={classes.name} gutterBottom variant="h5" component="h2">
                    {startup.name}
                </Typography>
                <Typography className={classes.segment} gutterBottom>
                    {startup.Segment?.name}
                </Typography>
                <DialogContentText id="alert-dialog-description">
                    {startup.description}
                </DialogContentText>
                <div className={classes.ratingGroup}>
                    <Typography className={classes.criterion}>Proposta</Typography>
                    <Rating
                        name='proposal'
                        value={evaluation.proposal}
                        onChange={(event) => {
                            setEvaluation({ ...evaluation, proposal: parseInt(event.target.value) });
                        }}
                    />
                    <Typography className={classes.criterion}>Apresentação / Pitch</Typography>
                    <Rating
                        name='pitch'
                        value={evaluation.pitch}
                        onChange={(event) => {
                            setEvaluation({ ...evaluation, pitch: parseInt(event.target.value) });
                        }}
                    />
                    <Typography className={classes.criterion}>Desenvolvimento</Typography>
                    <Rating
                        name='development'
                        value={evaluation.development}
                        onChange={(event) => {
                            setEvaluation({ ...evaluation, development: parseInt(event.target.value) });
                        }}
                    />
                 </div>
            </DialogContent>
            <DialogActions>
            <Button onClick={handleClose} color="primary">
                Cancelar
            </Button>
            <Button onClick={handleSave} color="primary">
                Salvar
            </Button>
            </DialogActions>
      </Dialog>
    )
};

StartupDialog.propTyes = {
    open: PropTypes.bool.isRequired,
    startup: PropTypes.object.isRequired,
    onClose: PropTypes.func.isRequired,
}

export default StartupDialog;