import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom';
import Home from '../../pages/Home';
import Results from '../../pages/Results';

const Routes = () => (
    <Router>
        <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/results' component={Results} />
        </Switch>
    </Router>
)

export default Routes;