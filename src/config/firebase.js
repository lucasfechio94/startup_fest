import firebase from 'firebase';

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyD4LYZ-UyMfq8XQt_SW1ZLztJYm4gbJol4",
    authDomain: "startup-fest-b63f7.firebaseapp.com",
    databaseURL: "https://startup-fest-b63f7.firebaseio.com",
    projectId: "startup-fest-b63f7",
    storageBucket: "startup-fest-b63f7.appspot.com",
    messagingSenderId: "397850914503",
    appId: "1:397850914503:web:9c9d9bc7a6a1aed3c118e2",
    measurementId: "G-CCHS3758CE"
});

const db = firebaseApp.firestore();

export { db };