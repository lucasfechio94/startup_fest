import React, { useState } from 'react';
import { 
    Card,
    CardContent,
    CardMedia,
    CircularProgress,
    Grid,
    Typography,
    Container
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import StartupDialog from '../../components/StartupDialog';

const useStyles = makeStyles((theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  titleContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    cursor: 'pointer',

    '&:hover': {
      boxShadow: '3px 3px 3px 3px #ccc',
    },
  },
  cardMedia: {
    paddingTop: '56.25%',
  },
  cardContent: {
    flexGrow: 1,
  },
}));

const GET_STARTUPS = gql`
{
    allStartups {
      name,
      imageUrl,
      description,
      Segment {
        name
      }
    }
}`

const Home = () => {
  const classes = useStyles();
  const { data, loading } = useQuery(GET_STARTUPS);
  const [open, setOpen] = useState(false);
  const [selectedStartup, setSelectedStartup] = useState({});

  const handleSelectStartup = ( startup ) => {
    setSelectedStartup(startup);
    setOpen(true);
  };
  
  return (
    <> 
      <div className={classes.titleContent}>
        <Container maxWidth="sm">
          <Typography component="h4" variant="h4" align="center" color="textPrimary">
            Escolha sua StartUp!
          </Typography>
        </Container>
      </div>
      <Container className={classes.cardGrid} maxWidth="md">
        {
          loading
            ? <CircularProgress />
            : (
                <>
                  <Grid container spacing={4}>
                    {
                      data &&
                      data.allStartups.map((startup) => (
                        <Grid item key={startup.name} xs={12} sm={6} md={4}>
                          <Card onClick={() => handleSelectStartup(startup)} className={classes.card}>
                            <CardMedia
                              className={classes.cardMedia}
                              image={startup.imageUrl}
                            />
                            <CardContent className={classes.cardContent}>
                              <Typography gutterBottom variant="h5" component="h2">
                                {startup.name}
                              </Typography>
                              <Typography>
                                {startup.Segment.name}
                              </Typography>
                            </CardContent>
                          </Card>
                        </Grid>
                      ))
                    }
                  </Grid>
                  <StartupDialog 
                    open={open}
                    onClose={() => setOpen(false)}
                    startup={selectedStartup}
                  />
                </>
            )
        }
      </Container>
    </>
  );
}

export default Home;
