import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Container,
    Typography,
    CircularProgress,
} from '@material-ui/core';
import { db } from '../../config/firebase';
import Ranking from '../../components/Ranking';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    justifyContent: 'center',

    '@media (max-width:1024px)': {
        flexDirection: 'column',
    }
  },
  titleContainer: {
    padding: theme.spacing(8, 0, 6),
  },
}));


const Results = () => {
  const classes = useStyles();
  const [proposal, setProposal] = useState([]);
  const [pitch, setPitch] = useState([]);
  const [development, setDevelopment] = useState([]);
  const [loading, setLoading] = useState(true);

  const getProposal = async () => {
    const ref = await db.collection('startups').orderBy('proposal.evaluation', 'desc').limit(3).get();
    const startups = await Promise.all(ref.docs.map(doc => doc.data()));
    setProposal(startups);
  };

  const getPitch = async () => {
    const ref = await db.collection('startups').orderBy('pitch.evaluation', 'desc').limit(3).get();
    const startups = await Promise.all(ref.docs.map(doc => doc.data()));
    setPitch(startups);
  };

  const getDevelopment = async () => {
    const ref = await db.collection('startups').orderBy('development.evaluation', 'desc').limit(3).get();
    const startups = await Promise.all(ref.docs.map(doc => doc.data()));
    setDevelopment(startups);
    setLoading(false);
  };

  useEffect(() => {
    getProposal();
    getPitch();
    getDevelopment();
  }, []);
  
  return (
    <>
        <Container className={classes.titleContainer} maxWidth="sm">
            <Typography component="h4" variant="h4" align="center" color="textPrimary">
                Resultados
            </Typography>
        </Container>
        {
            loading
                ? <CircularProgress />
                : (
                    <div className={classes.container}>
                        <Ranking
                            title='Proposta'
                            startups={proposal}
                        />
                        <Ranking
                            title='Apresentação / Pitch'
                            startups={pitch}
                        />
                        <Ranking
                            title='Desenvolvimento'
                            startups={development}
                        />
                    </div>
                )
        }
    </>
  );
}

export default Results;