import React from 'react';
import './App.css';
import Menu from '../src/components/Menu';
import Routes from '../src/shared/routes';

function App() {
  return (
    <div className="App">
      <Menu />
      <Routes />
    </div>
  );
}

export default App;
